##########################################################################################################
#                                            STEP 1: import packages                                     #
##########################################################################################################
import os
from os import path
import requests
import pandas as pd
from io import StringIO
from datetime import datetime
import subprocess
import functools
from matplotlib import pyplot as plt


############################## PDF LIBRARIES ##################################
from matplotlib.backends.backend_pdf import PdfPages

# ReportLab for pdf generation
try:
    import reportlab as rl
    print('ReportLab already installed, only imported')
except:
    import sys

    subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'reportlab'])
    import reportlab as rl
    print('ReportLab was not installed, installed and imported')

# BytesIO to convert img data to bytes for processing into PDF
try:
    from cStringIO import StringIO as BytesIO
except ImportError:
    from io import BytesIO

# pdfrw for read/write PDF operations
try:
    import pdfrw
    print('pdfrw already installed, only imported')
except:
    import sys
    
    subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'pdfrw'])
    import pdfrw
    print('pdfrw was not installed, installed and imported')

### import ipywidgets
from IPython.display import display, clear_output
import ipywidgets as widgets
from ipywidgets import interact, interactive, fixed, interact_manual, Button, Layout, jslink, IntText, IntSlider, \
    interactive_output

### import reportlab
from reportlab.pdfgen import canvas
from reportlab.lib.units import cm
from reportlab.platypus import SimpleDocTemplate, Paragraph, Table, TableStyle, Image, Spacer, Flowable, PageBreak
from reportlab.lib.styles import ParagraphStyle, getSampleStyleSheet
from reportlab.lib.utils import ImageReader

### import pdfrw
from pdfrw import PdfReader, PdfDict
from pdfrw.buildxobj import pagexobj
from pdfrw.toreportlab import makerl


#############################################STEP 1B: define classes #####################################
class PdfImage(Flowable):
    """
    Generates a reportlab image flowable for matplotlib figures. It is initialized
    with either a matplotlib figure or a pointer to a list of pagexobj objects and
    an index for the pagexobj to be used.
    """

    def __init__(self, fig=None, width=200, height=200, cache=None, cacheindex=0):
        self.img_width = width
        self.img_height = height
        if fig is None and cache is None:
            raise ValueError("Either 'fig' or 'cache' must be provided")
        if fig is not None:
            imgdata = BytesIO()
            fig.savefig(imgdata, format='pdf')
            imgdata.seek(0)
            page, = PdfReader(imgdata).pages
            image = pagexobj(page)
            self.img_data = image
        else:
            self.img_data = None
        self.cache = cache
        self.cacheindex = cacheindex

    def wrap(self, width, height):
        return self.img_width, self.img_height

    def drawOn(self, canv, x, y, _sW=0):
        if _sW > 0 and hasattr(self, 'hAlign'):
            a = self.hAlign
            if a in ('CENTER', 'CENTRE', TA_CENTER):
                x += 0.5 * _sW
            elif a in ('RIGHT', TA_RIGHT):
                x += _sW
            elif a not in ('LEFT', TA_LEFT):
                raise ValueError("Bad hAlign value " + str(a))
        canv.saveState()
        if self.img_data is not None:
            img = self.img_data
        else:
            img = self.cache[self.cacheindex]
        if isinstance(img, PdfDict):
            xscale = self.img_width / img.BBox[2]
            yscale = self.img_height / img.BBox[3]
            canv.translate(x, y)
            canv.scale(xscale, yscale)
            canv.doForm(makerl(canv, img))
        else:
            canv.drawImage(img, x, y, self.img_width, self.img_height)
        canv.restoreState()
        

class PdfImageCache(object):
    """
    Saves matplotlib figures to a temporary multi-page PDF file using the 'savefig'
    method. When closed the images are extracted and saved to the attribute 'cache'.
    The temporary PDF file is then deleted. The 'savefig' returns a PdfImage object
    with a pointer to the 'cache' list and an index for the figure. Use of this
    cache reduces duplicated resources in the reportlab generated PDF file.

    Use is similar to matplotlib's PdfPages object. When not used as a context
    manager, the 'close()' method must be explictly called before the reportlab
    document is built.
    """

    def __init__(self):
        self.pdftempfile = '_temporary_pdf_image_cache_.pdf'
        self.pdf = PdfPages(self.pdftempfile)
        self.cache = []
        self.count = 0

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.close()

    def close(self, *args):
        self.pdf.close()
        pages = PdfReader(self.pdftempfile).pages
        pages = [pagexobj(x) for x in pages]
        self.cache.extend(pages)
        os.remove(self.pdftempfile)

    def savefig(self, fig, width=200, height=200):
        self.pdf.savefig(fig)
        index = self.count
        self.count += 1
        return PdfImage(width=width, height=height, cache=self.cache, cacheindex=index)


class MCLine(Flowable):
    """
    Line flowable --- draws a line in a flowable
    http://two.pairlist.net/pipermail/reportlab-users/2005-February/003695.html
    """
    #----------------------------------------------------------------------
    def __init__(self, width, height=0):
        Flowable.__init__(self)
        self.width = width
        self.height = height
    #----------------------------------------------------------------------
    def __repr__(self):
        return "Line(w=%s)" % self.width
    #----------------------------------------------------------------------
    def draw(self):
        """
        draw the line
        """
        self.canv.line(0, self.height, self.width, self.height)


##########################################################################################################
#                                            STEP 2: create all functions                                #
##########################################################################################################

#############################################STEP 2A: functions for widgets #############################




############################################# STEP 2B: functions for PDF generation ######################
# This function only saves given figures to a PDF file, with each figure making up a new page of the combined file.
# Uses native functions from  matplotlib library
def figs_to_pdf(figs, filename=''):
    print('INFO: Saving figures into PDF...')
    now = datetime.now()
    # Remove milliseconds using the split built in function
    date_and_time_now = str(now).split('.')[0]
    
    # if filename not set: give default filename with current date/time
    if not filename:
        filename = "Analysis Figures: " + date_and_time_now + ".pdf"
    
    # if file already exists on storage, request new filename
    if (path.exists(filename)):
        print('INFO: Filename already taken. Please enter a different filename and try again.')
    else:
        # Create the PdfPages object to which we will save the pages:
        # The with statement makes sure that the PdfPages object is closed properly at
        # the end of the block, even if an Exception occurs.
        with PdfPages(filename) as pdf:
            # if list of figs is empty, no analysis has been generated: generate new analysis.
            # or if athletes in picker are not same as athletes in analysis
            for fig in figs:
                pdf.savefig(fig)

        if (path.exists(filename)):
            print('INFO: Figures PDF saved.')
        else:
            print('INFO: Something went wrong while saving the PDF, please try again.')


# This functions saves given figures into a PDF report that is saved under [filename]. The figures are automatically divided over the pages, meaning no 'half-figure' will exist on any one page.
# This function uses the ReportLab library:: https://www.reportlab.com/docs/reportlab-userguide.pdf
def figs_to_pdf_report(figs, logo_url, filename='', subject = 'This is a test subject'):
    print('INFO: Generating PDF Report...')
    now = datetime.now()
    
    # Remove milliseconds using the split built in function
    date_and_time_now = str(now).split('.')[0]
    
    if not filename:
        filename = "Analysis Report: " + date_and_time_now + ".pdf"        
    
    # Define logo dimensions
    logo_width = 150
    logo_height = 150
    
    # if file already exists on storage, request new filename
    if (path.exists(filename)):
        print('ERROR: Filename already taken. Please enter a different filename and try again.')
    else:
        # Create document
        doc = SimpleDocTemplate(filename)

        # Import stylesheet
        styles = getSampleStyleSheet()

        # Initialize list of elements that will be built into PDF document
        report = [Spacer(0, cm)]

        ###### TITLE PAGE #####
        # Display logo's next to eachother (requires table flowable)
        table_data = [
            [Image(logo_url,
                            width=logo_width, height=logo_height),
             Image('https://pbs.twimg.com/profile_images/1259760961839390721/fculmySf_400x400.jpg',  # <== SDV logo
                            width=logo_width, height=logo_height)]
        ]
        
        table = Table(table_data)
        report.append(table)
        report.append(Spacer(1, 24))

        # Draw *custom line flowable* under logo's
        line = MCLine(450)
        report.append(line)
        report.append(Spacer(1, 24))

        # title
        title = "Rapportage"
        report.append(Paragraph(title, styles['Title']))
        report.append(Spacer(1, 375))

        # date & time
        text = f'Gegenereerd op: {date_and_time_now}'
        report.append(Paragraph(text, styles['Normal']))
        report.append(PageBreak())  # skip to next page

        # subject
        text = f"Analyse voor {subject}"
        report.append(Paragraph(text, styles['Heading2']))
        report.append(Spacer(1, 24))


        ###### Report ######
        if not figs:
            print('ERROR: List of figures is empty.')
        else:
            report = make_report_cached_figs(report, figs)

            ###### BUILD PDF #####
            doc.build(report)

            print(f'INFO: Report {filename} saved.')


def make_report_cached_figs(report, figs):
    """
    Returns a 'story' (=report variable) with figs of matplotlib plots using PdfImageCache
    to reduce PDF file size.
    """
    with PdfImageCache() as pdfcache:
        report.append(Spacer(1, 0.5 * cm))

        for fig in figs:
            img = pdfcache.savefig(fig, width=450, height=150)

            report.append(img)
            report.append(Spacer(1, 24))

    return report

############################################# STEP 2C: functions for displaying figures ##################
def display_figures(figs):
    
    for fig in figs:
        display(fig)
    
    print('INFO: Figures displayed.')


##########################################################################################################
#                                            STEP 3: execute script function (main, start)               #
##########################################################################################################
#  Run program
figs_list = []
def execute_script(figs):
    global figs_list
    figs_list = figs
    ################# button 1 ################
    button1 = widgets.Button(description="Display Figures",
                             layout=widgets.Layout(width='30%', height='50px', border='5px blue'))
    output1 = widgets.Output()

    @output1.capture()
    def on_button_clicked1(b):
        with output1:
            clear_output()
            display_figures(figs)

    button1.on_click(functools.partial(on_button_clicked1))


    ################ button 2 ################
    button2 = widgets.Button(description="Generate Figures PDF (Figures only)",
                             layout=widgets.Layout(width='30%', height='50px', border='10px red'))

    @output1.capture()
    def on_button_clicked2(b):
        with output1:
            clear_output()
            figs_to_pdf(figs)

    button2.on_click(functools.partial(on_button_clicked2))


    ################ button 3 ################
    button3 = widgets.Button(description="Generate PDF Report",
                             layout=widgets.Layout(width='30%', height='50px', border='10px red'))
    
    @output1.capture()
    def on_button_clicked3(b):
        with output1:
            clear_output()
            figs_to_pdf_report(figs, 
                               logo_url='https://pbs.twimg.com/profile_images/1171789755513810944/aI49ff48_400x400.jpg')

    button3.on_click(functools.partial(on_button_clicked3))


    ################# display #################
    # display(widget x y z)
    display(button1, button2, button3, output1)
    print('')
    print('')
